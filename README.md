## [Flutter-Development-Roadmap](https://instacodeblog.com/roadmap-to-becoming-flutter-developer-updated-2021/#External_Libraries_for_Flutter_Developers)



### [Roadmap to Becoming Flutter Developer – Updated 2021](https://instacodeblog.com/roadmap-to-becoming-flutter-developer-updated-2021/#Conclusion)


#### What is Flutter?
Flutter is an open source UI softwere devlopment kit created by Google.It is used to develop cross plateform application for Android,IOS,Linux,Mac,Windows,Google Fuchsia,and the web form a single codebase.

                                  Where to start in Flutter?
It's A matter of 6months:
1. First Month-> Only UI Design(user interface):
   1. Login/Signup Page
   2. Profile Page
   3. Instagram UI
   4. Whats App UI
   5. Netflix UI
   6. Spotify UI
   7. E-commerce UI
2. 2nd Month -> BUILD LOGIC:
   1. Wire Framing/Paper Planing
   2. Writing Functions
   3. Calculator/Stopwatch App
   4. TODO App without Cloud Database
   5. Understand Dart Language Concepts (Async/Await)
3. 3rd Month -> Database:
   1. Firebase Authentication
   2. TODO App
   3. Chat App
   4. E-Commerce Cart
   5. Social Media Posting
4. 4th Month -> API:
   1.  Weather API
   2.  TMDB API
   3.  Spoonacular API
   4.  Create Models for Rendering JSON Data
   5.  Start with Provider State Management also
5. 5th Month -> Your own Backend + Flutter Web.
6. 6th Month -> Clean Architecture

..............................

                              10 Flutter Apps You Must Make as a Beginner | Flutter | App Development
1. Simple Calculator
2. Todo App
3. Currency Convertor
4. Weather Application
5. Wallpaper App Using API's 
6. News App
7. Movies App Using TMDB API
8. Ecommerce App
9. Clones (Instagram,whatsapp,facebook,youtube etc) 
10. ML app using Tflite

                                    flutter-app-development-roadmap
                                    
![flutter-app-development-roadmap-by-tarikul](https://user-images.githubusercontent.com/68488154/140614209-2d0d6f20-1323-4968-bbce-738e0e8929d9.png)

1. Programming Language
    1. Dart
   
2. IDE for development
    1. VsCode
    2. Android Studio
    3. intellij
   
3. User Interface
    1. Widgets
       1. statefull widget
       2. stateless widget
       3. accessibility
       4. Inherited widget
           1. Theming
           2. Localization
    2. Style
       1. Material
       2. Cupertion
    3. Assets
       1. fonts
       2. images
       3. svg
       4. audio
       5. video
       
4. Static User Interface
     1. View
        1. Text,Image,button raised button etc
     2. ViewGroup
        1. Container, Row, Column, Stack, Expanded, ConstrainedBox

5. Dynamic User Interface
     1. ListView
     2. GridView
     3. ExpansionTitle
     
6. Animation
     1. AnimatedWidget
     2. AnimatedBuilder
     3. AnimationController
     4. CurvedAnimation
     5. Hero
     6. Transform
     7. Opacity
     
7. Sotrage
     1. shared preference
     2. file storage
     3. sqlite
  
8. 3rd party libararies
     1. http
     2. dio
     3. get_it
     4. cached_network_image
     5. Flutter_webview_plug-in
     6. font_awesome_flutter
     7. SQFLite
     8. rxdart
     9. bloc_pattern
     
9. Behavior Components
     1. Permission
     2. Local Notification
     3. Push Notification
     4. Download Manager
     5. Media Playback
     6. Preference
     7. Sharing
     
10. State management
     1. setState
     2. Provider
     3. Redux
     4. BLoC
     5. MobX
  
11. Quality Assurance
     1. Firebase
        1. Crashlytics
        2. App distribution
        3. Analytics
     2. Google play beta tests
     3. TestFlight
     4. App Center

12. Version Control
     1. Git
     2. Github
     3. Bitbucket
     4. Gitlab

13. Firebase
     1. Firebase Auth
     2. Firebase database
     3. Firebase Storage
     4. Firebase Messaging

14. Native Integration
     1. Android
        1. Android Studio
        2. Java
        3. Kotlin
        4. App Siging
        5. Google Play Store
        6. In App Purchase

     2. ios
        1. Xcode
        2. Swift
        3. Objective-C
        4. Apple Certification
        5. AppStore

Keep Learning and try to improve your code.
